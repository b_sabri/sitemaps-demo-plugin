<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since      1.0.0
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 * @author     Sabri Bouchaala <bouchaala.sabri@gmail.com>
 */
class Artists_Albums_And_Songs_Sitemaps {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Artists_Albums_And_Songs_Sitemaps_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The Title of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used as a title for this plugin.
	 */
	protected $plugin_title;


	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ARTISTS_ALBUMS_AND_SONGS_SITEMAPS_VERSION' ) ) {
			$this->version = ARTISTS_ALBUMS_AND_SONGS_SITEMAPS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'artists-albums-and-songs-sitemaps';
		$this->plugin_title = "Artists Custom Sitemaps";		

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Artists_Albums_And_Songs_Sitemaps_Loader. Orchestrates the hooks of the plugin.
	 * - Artists_Albums_And_Songs_Sitemaps_i18n. Defines internationalization functionality.
	 * - Artists_Albums_And_Songs_Sitemaps_Admin. Defines all hooks for the admin area.
	 * - Artists_Albums_And_Songs_Sitemaps_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-artists-albums-and-songs-sitemaps-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-artists-albums-and-songs-sitemaps-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-artists-albums-and-songs-sitemaps-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-artists-albums-and-songs-sitemaps-public.php';

		/**
		 * Those are external classes cloned from another plugin that manage custom data about artists, albums and songs.
		 */		

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/external/class.artist.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/external/class.album.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/external/class.song.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/php-sitemap-generator/SitemapGenerator.php';		


		$this->loader = new Artists_Albums_And_Songs_Sitemaps_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Artists_Albums_And_Songs_Sitemaps_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Artists_Albums_And_Songs_Sitemaps_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Artists_Albums_And_Songs_Sitemaps_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menus' );
		$this->loader->add_action( 'wp_ajax_ajax_load_all_artists', $plugin_admin, 'ajax_load_all_artists' );
		$this->loader->add_action( 'wp_ajax_ajax_process_artist', $plugin_admin, 'ajax_process_artist' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Artists_Albums_And_Songs_Sitemaps_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The title of the plugin used to to display option page header
	 *
	 * @since     1.0.0
	 * @return    string    The title of the plugin.
	 */
	public function get_plugin_title() {
		return $this->plugin_title;
	}	




	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Artists_Albums_And_Songs_Sitemaps_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
