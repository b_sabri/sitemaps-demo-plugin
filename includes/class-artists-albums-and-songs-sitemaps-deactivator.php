<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since      1.0.0
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 * @author     Sabri Bouchaala <bouchaala.sabri@gmail.com>
 */
class Artists_Albums_And_Songs_Sitemaps_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
