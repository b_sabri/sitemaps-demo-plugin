<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since      1.0.0
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/includes
 * @author     Sabri Bouchaala <bouchaala.sabri@gmail.com>
 */
class Artists_Albums_And_Songs_Sitemaps_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'artists-albums-and-songs-sitemaps',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
