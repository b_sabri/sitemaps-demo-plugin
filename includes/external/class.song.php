<?php
/**
 * ExternalSong Class imported and cleaned from another plugin for demo purpose.
 */

class ExternalSong {
    public $id;
    public $slug;
    public $permalink;
    public $album;
    public $artist;
    public $title;
    
    
    public function __construct( $id = null ) {
        if ( isset( $id ) ) {
            $this->song = self::get_by_id( $id );
            if ( $this->song == false ) {
                return false;
            } //$this->song == false
            $this->album     = self::get_album_from_song( $id );
            $this->artist    = ExternalAlbum::get_artist_from_album( $this->album->id );
            $this->id        = $this->song->id;
            $this->title     = $this->song->song_title;
            $this->slug      = $this->maybe_raptv_create_slug();
            $this->permalink = $this->get_permalink();
        } //isset( $id )
    }
    
    public static function fromRow( $song, $album = null, $artist = null ) {
        $new_song            = new ExternalSong();
        $new_song->album     = $album;
        $new_song->artist    = $artist;
        $new_song->id        = $song->id;
        $new_song->song      = $song;
        $new_song->title     = $new_song->song->song_title;
        $new_song->slug      = $new_song->maybe_raptv_create_slug();
        $new_song->permalink = $new_song->get_permalink();
        
        return $new_song;
    }
    
    public static function get_by_id( $id ) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM songs WHERE id=%d LIMIT 0,1", $id ) );
        if ( !$result )
            return false;
        return $result;
    }
    
    public function maybe_raptv_create_slug() {
        global $wpdb;
        
        if ( $this->song->song_slug != '' ) {
            return $this->song->song_slug;
        } //$this->song->song_slug != ''
        
        $slug = sanitize_title( $this->artist->slug ) . '-' . urldecode( sanitize_title( $this->title ) ) . '-lyrics';
        
        $slugs = $wpdb->get_results( "SELECT song_slug FROM songs WHERE song_slug LIKE '$slug%' AND id<>$this->id" );
        
        $rowcount = $wpdb->num_rows;
        if ( $slugs ) {
            if ( $rowcount > 0 ) {
                foreach ( $slugs as $row ) {
                    $data[] = $row->song_slug;
                } //$slugs as $row
                if ( in_array( $slug, $data ) ) {
                    $count = 0;
                    while ( in_array( ( $slug . '-' . ++$count ), $data ) );
                    $slug = $slug . '-' . $count;
                } //in_array( $slug, $data )
            } //$rowcount > 0
        } //$slugs
        if ( empty( $result->song_slug ) ) {
            $wpdb->query( "UPDATE songs SET song_slug = '$slug' WHERE id = " . $this->id );
        } //empty( $result->song_slug )
        return $slug;
    }
    
    public function get_permalink() {
        $permalink = home_url() . '/songs/' . $this->slug . '/';
        return $permalink;
    }
    
    public static function get_album_from_song( $id ) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT albums.* FROM songs, albums where albums.id = songs.album_id AND songs.id = $id" ) );
        $album  = ExternalAlbum::fromRow( $result );
        return $album;
    }
       
}