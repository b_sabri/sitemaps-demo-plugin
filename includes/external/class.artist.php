<?php
/**
 * ExternalArtist Class imported and cleaned from another plugin for demo purpose.
 */
class ExternalArtist {
    public $id;
    public $slug;
    public $permalink;
    public $artist;
    
    public function __construct( $id = null ) {
        if ( isset( $id ) ) {
            $this->artist    = self::get_by_id( $id );
            $this->id        = $this->artist->id;
            $this->slug      = str_replace( '@', '', $this->artist->tag );
            $this->permalink = $this->get_permalink();
        } //isset( $id )
    }
    
    public static function fromRow( $artist ) {
        $new_artist            = new ExternalArtist();
        $new_artist->artist    = $artist;
        $new_artist->id        = $artist->id;
        $new_artist->slug      = str_replace( '@', '', $artist->tag );
        $new_artist->permalink = $new_artist->get_permalink();
        
        return $new_artist;
    }
    
    public static function get_by_id( $id ) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM artists WHERE id=%d LIMIT 0,1", $id ) );
        return $result;
    }
    
    public function get_permalink() {
        $permalink = home_url() . '/artists/' . str_replace( '@', '', $this->slug ) . '/';
        return $permalink;
    }
    
    public static function get_all_artists() {
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM artists limit 0 , 1000" );
        return $result;
    }
    
    public function get_all_albums() {
        return ExternalAlbum::get_all_albums( $this->id );
    }
}