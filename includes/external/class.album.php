<?php
/**
 * ExternalAlbum Class imported and cleaned from another plugin for demo purpose.
 */
class ExternalAlbum {
    public $id;
    public $slug;
    public $permalink;
    public $album;
    public $artist;
    
    public function __construct( $id = null ) {
        if ( isset( $id ) ) {
            $this->album     = self::get_by_id( $id );
            $this->artist    = self::get_artist_from_album( $id );
            $this->id        = $this->album->id;
            $this->slug      = $this->maybe_raptv_create_slug();
            $this->permalink = $this->get_permalink();
        } //isset( $id )
    }
    
    public static function fromRow( $album ) {
        $new_album            = new ExternalAlbum();
        $new_album->album     = $album;
        $new_album->artist    = self::get_artist_from_album( $new_album->album->id );
        $new_album->id        = $new_album->album->id;
        $new_album->slug      = $new_album->maybe_raptv_create_slug();
        $new_album->permalink = $new_album->get_permalink();
        
        return $new_album;
    }
    
    public static function get_by_id( $id ) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM albums WHERE id=%d LIMIT 0,1", $id ) );
        return $result;
    }
    
    public function maybe_raptv_create_slug() {
        global $wpdb;
        
        if ( $this->album->slug != '' ) {
            return $this->album->slug;
        } //$this->album->slug != ''
        
        $slug     = sanitize_title( $this->album->title );
        $slugs    = $wpdb->get_results( "SELECT  slug FROM albums WHERE slug LIKE '$slug%' AND id<>$this->id" );
        $rowcount = $wpdb->num_rows;
        if ( $slugs ) {
            if ( $rowcount > 0 ) {
                foreach ( $slugs as $row ) {
                    $data[] = $row->slug;
                } //$slugs as $row
                if ( in_array( $slug, $data ) ) {
                    $count = 0;
                    while ( in_array( ( $slug . '-' . ++$count ), $data ) );
                    $slug = $slug . '-' . $count;
                } //in_array( $slug, $data )
            } //$rowcount > 0
        } //$slugs
        
        if ( empty( $result->slug ) ) {
            $wpdb->query( "UPDATE albums SET slug = '$slug' WHERE id = " . $this->id );
        } //empty( $result->slug )
        return $slug;
    }
    
    public function get_permalink() {
        $permalink = home_url() . '/albums/' . $this->artist->slug . '/' . $this->slug . '/';
        return $permalink;
    }
    
    public static function get_all_albums( $artist_id = null ) {
        global $wpdb;
        if ( isset( $artist_id ) && !empty( $artist_id ) ) {
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT albums.* FROM albums, artists where artists.id = albums.artist_id AND artists.id = %d", $artist_id ) );
        } //isset( $artist_id ) && !empty( $artist_id )
        else {
            $result = $wpdb->get_results( "SELECT albums.* FROM albums" );
        }
        
        $albums = array();
        
        foreach ( $result as $key => $album ) {
            $albums[] = self::fromRow( $album );
        } //$result as $key => $album
        
        return $albums;
        
    }
    
    public function get_all_songs() {
        global $wpdb;
        $result = $wpdb->get_results( $wpdb->prepare( "SELECT songs.* FROM songs, albums where albums.id = songs.album_id AND albums.id = %d", $this->id ) );
        
        $songs = array();
        
        foreach ( $result as $key => $song ) {
            $songs[] = ExternalSong::fromRow( $song, $this, $this->artist );
        } //$result as $key => $song
        
        return $songs;
    }
    
    public static function get_artist_from_album( $id ) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT artists.* FROM albums, artists where artists.id = albums.artist_id AND albums.id = %d", $id ) );
        $artist = ExternalArtist::fromRow( $result );
        return $artist;
    }
}
