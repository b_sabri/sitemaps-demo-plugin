<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since             1.0.0
 * @package           Artists_Albums_And_Songs_Sitemaps
 *
 * @wordpress-plugin
 * Plugin Name:       Artists Albums & Songs Sitemaps
 * Plugin URI:        https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Sabri Bouchaala
 * Author URI:        https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       artists-albums-and-songs-sitemaps
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'ARTISTS_ALBUMS_AND_SONGS_SITEMAPS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-artists-albums-and-songs-sitemaps-activator.php
 */
function activate_artists_albums_and_songs_sitemaps() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-artists-albums-and-songs-sitemaps-activator.php';
	Artists_Albums_And_Songs_Sitemaps_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-artists-albums-and-songs-sitemaps-deactivator.php
 */
function deactivate_artists_albums_and_songs_sitemaps() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-artists-albums-and-songs-sitemaps-deactivator.php';
	Artists_Albums_And_Songs_Sitemaps_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_artists_albums_and_songs_sitemaps' );
register_deactivation_hook( __FILE__, 'deactivate_artists_albums_and_songs_sitemaps' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-artists-albums-and-songs-sitemaps.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_artists_albums_and_songs_sitemaps() {

	$plugin = new Artists_Albums_And_Songs_Sitemaps();
	$plugin->run();

}
run_artists_albums_and_songs_sitemaps();
