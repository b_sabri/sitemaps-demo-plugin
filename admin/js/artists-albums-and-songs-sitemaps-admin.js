jQuery(document).ready(function($) {

  var $option_page_wrapper = $("#artists-albums-and-songs-sitemaps-content")
  var $artists_list_wrapper = $('#artists-albums-and-songs-sitemaps-list-items');

  $('#loading-card').css('display', 'none');
  $('#progress-card').css('display', 'block');

  $($option_page_wrapper).on('click', 'button#start', function(event) {
    event.preventDefault();
    $(this).attr('disabled', 'disabled');
    // start processing queue!
    process(queue, 10, doEach, doDone);
  });

  function process(q, num, fn, done) {
    // remove a batch of items from the queue
    var items = q.splice(0, num),
      count = items.length;

    // no more items?
    if (!count) {
      // exec done callback if specified
      done && done();
      // quit
      return;
    }

    // loop over each item
    for (var i = 0; i < count; i++) {
      // call callback, passing item and
      // a "done" callback
      fn(items[i], function() {
        // when done, decrement counter and
        // if counter is 0, process next batch
        --count - 1 || process(q, num, fn, done);
      });
    }
  }

  $uniqid = $artists_list_wrapper.data('uniqid');

  let all_progress = 0;
  let increment = 100 / $all_artists.length;

  // create queue
  let queue = [];

  var processArtist = function(index, done) {

  	$('#artist-' + $all_artists[index].id + ' .mdl-spinner' ).addClass('is-active');

    jQuery.ajax({
      type: "post",
      dataType: "json",
      url: ajaxurl,
      data: {
        action: "ajax_process_artist",
        id: $all_artists[index].id,
        uniqid: $uniqid
      },
      success: function(response) {
        if (response.success == false) {
          queue.push(index);
        }
        if (response.success == true) {
          $global_progress_bar = $('#global .progressbar');
          $('.artists-progress-num').text(parseInt($('.artists-progress-num').text()) + 1);
          $('#artist-' + response.data.id + ' .progressbar').css('width', '100%');
          let current_prog = parseFloat($global_progress_bar[0].style.width);
          $global_progress_bar.css('width', current_prog + parseFloat(increment) + '%');          
        }
      },
      error: function(response) {
        queue.push(index);
      },
      complete: function() {
	  	$('#artist-' + $all_artists[index].id + ' .mdl-spinner' ).removeClass('is-active');
        done();
      },
    })
  }

  $.each($all_artists, function(index, val) {
    queue.push(index);
  });

  // a per-item action
  function doEach(item, done) {
    processArtist(item, done);
  }

  // an all-done action
  function doDone() {
    jQuery.ajax({
      type: "post",
      dataType: "json",
      url: ajaxurl,
      data: {
        action: "ajax_process_artist",
        last: "true",
        uniqid: $uniqid
      },
      success: function(response) {
        if (response.success) {}
      }
    });
  }
});
