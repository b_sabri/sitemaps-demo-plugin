<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since      1.0.0
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/admin/partials
 */
?>

<h2><?php echo $heading ?></h2>
<div id="<?php echo $plugin_id ?>-content">
  <div id="post-body" class="metabox-holder columns-2">
    <div class="snippet-group">
      <div class="<?php echo $plugin_id ?>-card-wide mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text">
            <?php echo $generate_text ?>
          </h2>
        </div>
        <div class="mdl-card__supporting-text">
          <div class="notice notice-warning">
            <?php echo $not_refresh_warning ?>
          </div>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <button id="start" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
            <?php echo $generate_button ?>
          </button>
        </div>
      </div>
      <div id="loading-card" class="<?php echo $plugin_id ?>-card-wide mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text"><span><?php echo $loading_artists_text ?></span></h2>
        </div>
        <div class="mdl-card__supporting-text">
          <div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>
        </div>
      </div>
      <div id="progress-card" class="<?php echo $plugin_id ?>-card-wide mdl-card mdl-shadow--2dp" style="display: none">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text"><span><?php echo $progress_text ?></span></h2>
        </div>
        <div class="mdl-card__supporting-text">
          <p><span class="artists-progress-num">0</span> / <?php echo count( $artists ) ?> <?php echo $artists_text ?></p> 
          <div id="global" class="mdl-progress mdl-js-progress"></div>
        </div>
        <ul id="<?php echo $plugin_id ?>-list-items" class="mdl-list" data-uniqid="<?php echo uniqid(); ?>">
          <?php foreach ( $artists as $artist_key => $artist ) : ?>
          <li id="artist-<?php echo $artist->id; ?>" class="mdl-list__item">
            <div class="mdl-list__item-primary-content">
              <?php echo $artist->name ?>
              <span class="mdl-spinner mdl-spinner--single-color mdl-js-spinner"></span>
            </div>
            <div class="mdl-progress mdl-js-progress"></div>
          </li>
          <?php  endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
  <br class="clear">
</div>
<script type="text/javascript">
  $all_artists = JSON.parse('<?php echo( addslashes( $artists_json ) ); ?>');
</script>