<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.upwork.com/o/profiles/users/~013091259e173dc310/
 * @since      1.0.0
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Artists_Albums_And_Songs_Sitemaps
 * @subpackage Artists_Albums_And_Songs_Sitemaps/admin
 * @author     Sabri Bouchaala <bouchaala.sabri@gmail.com>
 */
class Artists_Albums_And_Songs_Sitemaps_Admin {
    
    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {
        
        $this->plugin_name  = $plugin_name;
        $this->plugin_title = "Artists Custom Sitemaps";
        $this->version      = $version;

        //This will only works if Yoast SEO is installed
        add_filter( 'wpseo_sitemap_index', array( $this , 'add_sitemap_custom_items' ) , 10 , 1 );


    }
    
    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {
        
        if ( $this->plugin_name == get_admin_page_title() ) {
            wp_enqueue_style( $this->plugin_name . '-mdl', plugin_dir_url( __FILE__ ) . 'css/material.css', array(), $this->version, 'all' );
            wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/artists-albums-and-songs-sitemaps-admin.css', array(), $this->version, 'all' );
        } //$this->plugin_name == get_admin_page_title()
        
        
    }
    
    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        
        if ( $this->plugin_name == get_admin_page_title() ) {
            wp_enqueue_script( $this->plugin_name . '-mdl', plugin_dir_url( __FILE__ ) . 'js/material.js', array(
                 'jquery' 
            ), $this->version, false );
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/artists-albums-and-songs-sitemaps-admin.js', array(
                 'jquery',
                $this->plugin_name . '-mdl' 
            ), $this->version, false );
        } //$this->plugin_name == get_admin_page_title()
        
    }
    
    public function add_menus() {
        
        add_submenu_page( 'tools.php', $this->plugin_name, $this->plugin_title, 'manage_options', $this->plugin_name, array(
             $this,
            'render' 
        ) );
        
    }
    
    public function ajax_load_all_artists() {
        $results = ExternalArtist::get_all_artists();
        wp_send_json_success( $results );
    }
    
    public function ajax_process_artist() {
        
        if ( isset( $_REQUEST[ "last" ] ) && $_REQUEST[ "last" ] == "true" ) {
            $this->raptv_generate_sitemap();
            $response = array(
                 'all_done' => true 
            );
            wp_send_json_success( $response );
        } //isset( $_REQUEST[ "last" ] ) && $_REQUEST[ "last" ] == "true"
        
        global $wpdb;
        
        $artist_id = $_REQUEST[ "id" ];
        $uniqid    = $_REQUEST[ "uniqid" ];
        
        $upload_folder   = wp_get_upload_dir();
        $upload_folder   = $upload_folder[ "basedir" ];
        $sitemaps_folder = $upload_folder . "/temp-sitemaps/$uniqid";
        
        if ( !file_exists( $sitemaps_folder ) ) {
            mkdir( $sitemaps_folder, 0777, true );
        } //!file_exists( $sitemaps_folder )
        
        $artists_sitemap_temp_file = $sitemaps_folder . "/artists.txt";
        $albums_sitemap_temp_file  = $sitemaps_folder . "/albums.txt";
        $songs_sitemap_temp_file   = $sitemaps_folder . "/songs.txt";
        
        if ( !is_file( $artists_sitemap_temp_file ) ) {
            file_put_contents( $artists_sitemap_temp_file, '' );
        } //!is_file( $artists_sitemap_temp_file )
        
        if ( !is_file( $albums_sitemap_temp_file ) ) {
            file_put_contents( $albums_sitemap_temp_file, '' );
        } //!is_file( $albums_sitemap_temp_file )
        
        if ( !is_file( $songs_sitemap_temp_file ) ) {
            file_put_contents( $songs_sitemap_temp_file, '' );
        } //!is_file( $songs_sitemap_temp_file )
        
        $artist = new ExternalArtist( $artist_id );
        
        $sitemap_artists[] = $artist->permalink . "," . date( 'c' ) . ",daily,0.5";
        
        $albums = $artist->get_all_albums();
        
        foreach ( $albums as $album ) {
            
            $album_permalink  = $album->get_permalink();
            $sitemap_albums[] = $album_permalink . "," . date( 'c' ) . ",daily,0.5";
            
            $songs = $album->get_all_songs();
            
            foreach ( $songs as $song ) {
                usleep( 10 );
                $song_permalink  = $song->get_permalink();
                $sitemap_songs[] = $song_permalink . "," . date( 'c' ) . ",daily,0.5";
            } //$songs as $song
        } //$albums as $album
        
        $sitemap_artists[] = '';
        $sitemap_albums[]  = '';
        $sitemap_songs[]   = '';
        
        file_put_contents( $artists_sitemap_temp_file, implode( PHP_EOL, $sitemap_artists ), FILE_APPEND );
        file_put_contents( $albums_sitemap_temp_file, implode( PHP_EOL, $sitemap_albums ), FILE_APPEND );
        file_put_contents( $songs_sitemap_temp_file, implode( PHP_EOL, $sitemap_songs ), FILE_APPEND );
        
        $response = array(
             'id' => $artist_id 
        );
        wp_send_json_success( $response );
        die();
    }
    
    
    private function raptv_generate_sitemap() {
        
        $uniqid = $_REQUEST[ "uniqid" ];
        
        $upload_folder   = wp_get_upload_dir();
        $upload_folder   = $upload_folder[ "basedir" ];
        $sitemaps_folder = $upload_folder . "/temp-sitemaps/$uniqid";
        
        // create object
        $sitemap_artists                       = new SitemapGenerator( home_url(), get_home_path() );
        $sitemap_artists->createGZipFile       = true;
        $sitemap_artists->maxURLsPerSitemap    = 10000;
        $sitemap_artists->sitemapFileName      = "/sitemap-artists.xml";
        $sitemap_artists->sitemapIndexFileName = "/sitemap-artists-index.xml";
        
        $artists_sitemap_temp_file = $sitemaps_folder . "/artists.txt";
        $artists_list              = file_get_contents( $artists_sitemap_temp_file, true );
        
        $artists_list = explode( PHP_EOL, $artists_list );
        array_pop( $artists_list );
        $artists_list = array_map( function( $row ) {
            $row      = str_replace( "'", "", $row );
            $row      = explode( ',', $row );
            $row[ 0 ] = trailingslashit( $row[ 0 ] );
            return $row;
        }, $artists_list );
        
        $artists_list = array_values( $artists_list );
        
        $sitemap_artists->addUrls( $artists_list );
        
        $sitemap_albums                       = new SitemapGenerator( home_url(), get_home_path() );
        $sitemap_albums->createGZipFile       = true;
        $sitemap_albums->maxURLsPerSitemap    = 10000;
        $sitemap_albums->sitemapFileName      = "/sitemap-albums.xml";
        $sitemap_albums->sitemapIndexFileName = "/sitemap-albums-index.xml";
        
        $albums_sitemap_temp_file = $sitemaps_folder . "/albums.txt";
        $albums_list              = file_get_contents( $albums_sitemap_temp_file, true );
        
        $albums_list = explode( PHP_EOL, $albums_list );
        array_pop( $albums_list );
        
        $albums_list = array_map( function( $row ) {
            $row      = str_replace( "'", "", $row );
            $row      = explode( ',', $row );
            $row[ 0 ] = trailingslashit( $row[ 0 ] );
            return $row;
        }, $albums_list );
        
        $albums_list = array_values( $albums_list );
        $sitemap_albums->addUrls( $albums_list );
        
        $sitemap_songs                       = new SitemapGenerator( home_url(), get_home_path() );
        $sitemap_songs->createGZipFile       = true;
        $sitemap_songs->maxURLsPerSitemap    = 10000;
        $sitemap_songs->sitemapFileName      = "/sitemap-songs.xml";
        $sitemap_songs->sitemapIndexFileName = "/sitemap-songs-index.xml";
        
        $songs_sitemap_temp_file = $sitemaps_folder . "/songs.txt";
        $songs_list              = file_get_contents( $songs_sitemap_temp_file, true );
        
        $songs_list = explode( PHP_EOL, $songs_list );
        array_pop( $songs_list );
        
        $songs_list = array_map( function( $row ) {
            $row      = str_replace( "'", "", $row );
            $row      = explode( ',', $row );
            $row[ 0 ] = trailingslashit( $row[ 0 ] );
            return $row;
        }, $songs_list );
        
        $songs_list = array_values( $songs_list );
        $sitemap_songs->addUrls( $songs_list );
        
        $sitemap_artists->createSitemap();
        $sitemap_artists->writeSitemap();
        
        $sitemap_albums->createSitemap();
        $sitemap_albums->writeSitemap();
        
        $sitemap_songs->createSitemap();
        $sitemap_songs->writeSitemap();
        
    }
    
    public function render() {
        $domain               = $this->plugin_name;
        $heading              = __( $this->plugin_title, $domain );
        $plugin_id            = esc_attr__( $this->plugin_name, $domain );
        $generate_text        = __( 'Generate Sitemaps', $domain );
        $generate_button      = esc_attr__( 'Start', $domain );
        $not_refresh_warning  = __( "Do not refresh or close this page after starting.", $domain );
        $progress_text        = __( "Progress", $domain );
        $artists_text         = __( "Artists", $domain );
        $loading_artists_text = __( "Loading Artists", $domain );
        $artists              = ExternalArtist::get_all_artists();
        $artists_json         = json_encode( $artists );
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/artists-albums-and-songs-sitemaps-admin-display.php';
    }

	public function add_sitemap_custom_items ( $sitemap_custom_items ) {

		$custom_indexes = [ 'sitemap-artists.xml','sitemap-albums.xml','sitemap-songs.xml' , 'sitemap-artists-index.xml','sitemap-albums-index.xml','sitemap-songs-index.xml' ];

		foreach ( $custom_indexes as $key => $custom_index ) {
			if ( file_exists( $custom_index ) ) {
	  			 $sitemap_custom_items .= '<sitemap>
				<loc>' . home_url() . '/' . $custom_index . '</loc>
				<lastmod>' . date('c') . '</lastmod>
				</sitemap>';
			}
		}

		return $sitemap_custom_items;
	}    
    
}